import React from "react";
import { BrowserRouter, Route } from "react-router-dom";

import { Header } from "./layout";
import { Home, Adicionar, Editar } from "./view";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Route path="/" exact component={Home} />
        <Route path="/adicionar" component={Adicionar} />
        <Route path="/editar/:id" component={Editar} />
      </BrowserRouter>
    </div>
  );
}

export default App;
