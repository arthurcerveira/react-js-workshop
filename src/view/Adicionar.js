import React, { useState } from "react";
import axios from "axios";

const Adicionar = () => {
  const [nome, setNome] = useState("");
  const [email, setEmail] = useState("");
  const [msg, setMsg] = useState("");

  const onSubmit = async event => {
    event.preventDefault();

    if (nome === "") setMsg("Preencha o nome");
    else if (email === "") setMsg("Preencha o email");
    else {
      const result = await axios({
        method: "POST",
        data: {
          nome,
          email
        },
        url: "http://localhost:3001/users"
      });

      if (result.status >= 200 && result.status < 300)
        setMsg("Usuário adicionado com sucesso");
      else setMsg("Usuário não foi adicionado");
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <p>{msg}</p>
      <div>
        <span>Nome</span>
        <input
          type="name"
          name="nome"
          value={nome}
          onChange={event => setNome(event.target.value)}
        />
      </div>
      <div>
        <span>Email</span>
        <input
          type="email"
          name="email"
          value={email}
          onChange={event => setEmail(event.target.value)}
        />
        <button>Adicionar</button>
      </div>
    </form>
  );
};

export default Adicionar;
