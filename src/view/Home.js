import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const Home = () => {
  const [lista, setLista] = useState([]);

  useEffect(() => {
    axios({
      method: "GET",
      url: "http://localhost:3001/users"
    }).then(data => {
      setLista(data.data);
    });
  }, []);

  return (
    <table>
      <tr>
        <th>ID</th>
        <th>Nome</th>
        <th>Email</th>
        <th>Ação</th>
      </tr>
      {lista.map((item, key) => (
        <tr>
          <td>{item.id}</td>
          <td>{item.nome}</td>
          <td>{item.email}</td>
          <td>
            <Link to={"/editar/" + item.id}>Editar</Link>
          </td>
        </tr>
      ))}
    </table>
  );
};

export default Home;
