import React, { useState, useEffect } from "react";
import axios from "axios";

const Editar = ({ match }) => {
  const userID = match.params.id;
  const [nome, setNome] = useState("");
  const [email, setEmail] = useState("");
  const [msg, setMsg] = useState("");
  const [loading, setLoading] = useState("");

  useEffect(() => {
    axios({
      method: "GET",
      url: "https://localhost:3001/users?id=" + userID
    }).then(data => {
      if (data.data.lenght === 0) setMsg("Esse usuario nao existe");
      else {
        setNome(data.data[0].nome);
        setEmail(data.data[0].email);
      }
    });
  }, []);

  const onSubmit = async event => {
    event.preventDefault();

    if (nome === "") setMsg("Preencha o nome");
    else if (email === "") setMsg("Preencha o email");
    else {
      const result = await axios({
        method: "PUT",
        data: {
          nome,
          email
        },
        url: "http://localhost:3001/users/" + userID
      });

      if (result.status >= 200 && result.status < 300)
        setMsg("Usuário editado com sucesso");
      else setMsg("Usuário não foi editado");
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <p>{msg}</p>
      <div>
        <span>Nome</span>
        <input
          type="name"
          name="nome"
          value={nome}
          onChange={event => setNome(event.target.value)}
        />
      </div>
      <div>
        <span>Email</span>
        <input
          type="email"
          name="email"
          value={email}
          onChange={event => setEmail(event.target.value)}
        />
        <button>Adicionar</button>
      </div>
    </form>
  );
};

export default Editar;
