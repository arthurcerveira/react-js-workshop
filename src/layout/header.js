import React from "react";
import { Link } from "react-router-dom";
const Header = () => (
  <div className="header">
    <h2>CRUD</h2>
    <Link to="/adicionar">Adicionar</Link>
    <Link to="/">Voltar</Link>
  </div>
);

export default Header;
